import express from "express";
import bodyParser from "body-parser";
import fs from "fs";

const PORT = 8000;
const MUSIC_FILE = "music.json"
const BOOK_FILE = "book.json"

type Music = {
  id: number;
  title: string;
  artists: string[];
};

type Book = {
  id: number;
  title: string;
  authors: string[];
};

function getData(filename: string) {
  return JSON.parse(fs.readFileSync(filename).toString());
}

function setData(filename: string, data: string) {
  fs.writeFileSync(filename, data);
}

const app = express();

app.use(bodyParser.json());

app.route("/").get((req, res) => res.send("Express + TypeScript Server"));

app.route("/music")
  .get((req, res) => {
    res.send(getData(MUSIC_FILE));
  })
  .post((req, res) => {
    const musics = getData(MUSIC_FILE);
    const newMusic: Music = {
      id: Math.max(...musics.map((m: Music) => m.id)) + 1,
      title: req.body.title,
      artists: req.body.artists.split(",").map((n: string) => n.trim()),
    };
    setData(MUSIC_FILE, JSON.stringify([...musics, newMusic]));
    res.send([...musics, newMusic]);
  });

app.route("/book")
  .get((req, res) => {
    res.send(getData(BOOK_FILE))
  })
  .post((req, res) => {
    const books = getData(BOOK_FILE);
    const newBook: Book = {
      id: Math.max(...books.map((m: Book) => m.id)) + 1,
      title: req.body.title,
      authors: req.body.authors.split(",").map((n: string) => n.trim()),
    };
    setData(BOOK_FILE, JSON.stringify([...books, newBook]));
    res.send([...books, newBook]);
  });

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
